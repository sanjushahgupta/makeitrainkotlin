package com.example.berich

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.berich.ui.theme.BeRichTheme
import kotlin.math.absoluteValue
import androidx.compose.runtime.remember as remember1

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

                       Greeting()
                        ForUi()

        }
    }
}

@Composable
fun ForUi(){
    BeRichTheme {
        var moneyCounter = remember1 {
            mutableStateOf(0)
        }

        // A surface container using the 'background' color from the theme
        Surface( modifier= Modifier
            .fillMaxHeight()
            .fillMaxWidth(),color =Color(0XFF546E7A)) {

            Column(verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "MAKE IT RAIN",fontWeight = FontWeight.ExtraBold, fontSize = 30.sp,
                    color = Color.Black)
                if(moneyCounter.value>5000){
                    Text(text = "Sorry, Bank don't have sufficient amount. \n Your bank balance is $5000 ", style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = FontWeight.ExtraBold,
                        color = Color.Red


                    )
                    )
                }else {
                    Text(
                        text = "$ ${moneyCounter.value}", style = TextStyle(
                            fontSize = 50.sp,
                            fontWeight = FontWeight.ExtraBold,
                            color = Color.Black


                        )
                    )
                }
                Spacer(modifier = Modifier.height(130.dp))


                Circle(moneycounter = moneyCounter.value){
                    moneyCounter.value=it +100



                }
                Text(text = "Tap to increase money \n maximum $5000")


            }


        }
    }

}

@Composable
fun Circle(moneycounter:Int=0, updatemoneyCounter:(Int)->Unit){





    Card(modifier = Modifier
        .padding(3.dp)
        .size(105.dp)
        .clickable {
            updatemoneyCounter(moneycounter)

        }, shape = CircleShape

        , elevation = 4.dp) {
        Box(contentAlignment = Alignment.Center){
            Text(text = "Tap",style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.ExtraBold,
                color = Color.Black)
            )
        }
        


    }
    
    

}

@Composable
fun Greeting() {

    Text(text="MAKE IT RAIN",style = TextStyle(
        fontSize = 50.sp,
        fontWeight = FontWeight.ExtraBold,
        color = Color.Black))
}


@Preview(showBackground = false)
@Composable
fun DefaultPreview() {
    BeRichTheme {

    }
}